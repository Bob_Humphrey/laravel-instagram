<?php

namespace App\Actions;

use App\InstagramUser;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class GetLeadsAction
{
  public static function execute(): void
  {
    // Reset the current lead information in the InstagramUsers table.

    $instagramUsers = InstagramUser::select('id')->where('current_lead', true);

    $instagramUsers->each(function ($item) {
      $instagramUser = InstagramUser::find($item->id);
      if (
        !$instagramUser->ever_follower
        && !$instagramUser->ever_following
        && !$instagramUser->never_following
        && !$instagramUser->lead_rating
        && !$instagramUser->lead_reject
      ) {
        $instagramUser->delete();
      } else {
        $instagramUser->current_lead = false;
        $instagramUser->current_lead_stopping_point = false;
        $instagramUser->save();
      }
    });

    // Update InstagramUsers with information from the leads.txt file.

    $errorCount = 0;
    $readCycleCount = 0;
    $errors = [];
    $instagramUser = self::initializeLead();

    $leadsFile = Storage::get('data/leads.txt');
    $records = explode("\n", $leadsFile);

    foreach ($records as $record) {
      // Profile picture record
      if ($readCycleCount === 0) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $readCycleCount++;
          continue;
        } else {
          $errors[] = $record . ' - PROFILE PICTURE EXPECTED';
          $errorCount++;
          continue;
        }
      }

      // Lead name record
      if ($readCycleCount === 1) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $errors[] = $record . ' - USER NAME RECORD EXPECTED';
          $errorCount++;
          $readCycleCount = 1;
          $instagramUser = self::initializeLead();
          continue;
        } else {
          $instagramUser['user_name'] = $record;
          $readCycleCount++;
          continue;
        }
      }

      // Name record
      if ($readCycleCount === 2) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $errors[] = $record . ' - NAME RECORD EXPECTED';
          $errorCount++;
          $instagramUser['name'] = $instagramUser['user_name'];
          self::save($instagramUser);
          $instagramUser = self::initializeLead();
          $readCycleCount = 1;
          continue;
        } else {
          $instagramUser['name'] = $record;
          self::save($instagramUser);
          $readCycleCount = 0;
          continue;
        }
      }
    }
    return;
  }

  private static function initializeLead(): array
  {
    $instagramUser = [
      'name' => '',
      'user_name' => ''
    ];
    return $instagramUser;
  }

  private static function save($instagramUserTextFile): void
  {
    $instagramUser = InstagramUser::where('user_name', $instagramUserTextFile['user_name'])->first();

    if (empty($instagramUser)) {
      $instagramUser = new InstagramUser;
    }

    $instagramUser->name = $instagramUserTextFile['name'];
    $instagramUser->user_name = $instagramUserTextFile['user_name'];
    $instagramUser->current_lead = true;
    $instagramUser->save();
  }
}
