<?php

namespace App\Actions;

use App\Day;
use App\InstagramUser;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ComputeDaysToFollowAction
{

  public static function execute(): void
  {
    $days = [
      '1' => 0,
      '2' => 0,
      '3' => 0,
      '4' => 0,
      '5' => 0,
      '6' => 0,
      '7' => 0,
      '8' => 0,
      '9' => 0,
      '10' => 0,
      '11' => 0,
      'more' => 0,
      'never' => 0,
      'minus' => 0,
      'ever following' => 0,
      'followed back' => 0,
      'followed back percent' => 0
    ];

    $everFollowingCount = InstagramUser::where('ever_following', 1)->get()->count();

    $followedBack = InstagramUser::where('ever_following', 1)
      ->where('follower', 1)
      ->get()
      ->count();

    $followings = InstagramUser::where('ever_following', 1)->get();

    foreach ($followings as $following) {
      $followerDate = Carbon::createFromDate($following->started_follower_date);
      $followingDate = Carbon::createFromDate($following->started_following_date);

      $difference = $followerDate->diffInDays($followingDate) + 1;
      if ($difference < 1) {
        $days['minus']++;
        continue;
      }

      if ($difference > 11) {
        $days['more']++;
        continue;
      }

      $strDifference = strval($difference);
      $days[$strDifference]++;
    }

    $days['ever following'] = $everFollowingCount;
    $days['followed back'] = $followedBack;
    $days['followed back percent'] = number_format((($days['followed back'] / $days['ever following']) * 100), 0);

    DB::table('days')->truncate();
    $collection = collect($days);
    $collection->each(function ($item, $key) {
      $day = new Day;
      $day->key = $key;
      $day->value = $item;
      $day->save();
    });
  }
}
