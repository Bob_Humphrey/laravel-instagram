  <div class="w-full justify-center py-6">
    <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
      Leads
    </h2>

    <div class="">
      <a href={{ url('/refresh-leads') }} class="text-sm text-blue-500 mb-6">
        Refresh Leads
      </a>
    </div>

    <table class="w-full">
      @foreach ($instagramUsers as $instagramUser)
        @if ($loop->index % 14 === 0)
          <tr class="grid grid-cols-12 text-sm font-nunito_bold bg-blue-50 border-b border-gray-300">
            <th class="col-span-2 text-left py-3 pl-3">User</th>
            <th class="col-span-2 text-left py-3">User Name</th>
            <th class="col-span-1 text-center py-3">Follower</th>
            <th class="col-span-1 text-center py-3">Following</th>
            <th class="col-span-1 text-center py-3">Never</th>
            <th class="col-span-1 text-center py-3">Source</th>
            <th class="col-span-1 text-center py-3">A</th>
            <th class="col-span-1 text-center py-3">B</th>
            <th class="col-span-1 text-center py-3">C</th>
            <th class="col-span-1 text-center py-3">Reject</th>
          </tr>
        @endif
        <tr
          class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? 'bg-gray-50' : '' }}">
          <td class="col-span-2 text-left py-3 pl-3">
            {{ $instagramUser->name }}
          </td>
          <td class="col-span-2 text-left py-3">
            <a href="https://www.instagram.com/{{ $instagramUser->user_name }}/" class="text-blue-500 cursor-pointer"
              target="_blank" rel="noopener noreferrer">
              {{ $instagramUser->user_name }}
            </a>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->ever_follower ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->ever_following ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->never_following ? 'bg-red-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="toggleLeadSource({{ $instagramUser->id }})"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_source ? 'bg-green-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="updateLeadRating({{ $instagramUser->id }}, 1)"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_rating === 1 ? 'bg-green-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="updateLeadRating({{ $instagramUser->id }}, 2)"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_rating === 2 ? 'bg-lime-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="updateLeadRating({{ $instagramUser->id }}, 3)"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_rating === 3 ? 'bg-orange-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="toggleLeadReject({{ $instagramUser->id }})"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_reject ? 'bg-red-500' : 'bg-gray-300' }}">
            </div>
          </td>
        </tr>
      @endforeach
    </table>
  </div>
