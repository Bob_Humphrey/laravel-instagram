<?php

namespace App\Http\Livewire;

use App\InstagramUser;
use Livewire\Component;
use Illuminate\View\View;
use Illuminate\Support\Carbon;

class Followers extends Component
{

  public $state = [
    'followers' => [],
    'followers_count' => 0,
    'add_following_count' => 0,
    'sort_field' => 'started_follower_date',
    'sort_direction' => 'DESC',
    'sort_next' => [
      'name' => 'ASC',
      'user_name' => 'ASC',
      'add_following' => 'DESC',
      'never_following' => 'DESC',
      'favorite' => 'DESC',
      'active_liker' => 'DESC',
      'lead_source' => 'DESC',
      'lead_source_used' => 'DESC',
      'started_follower_date' => 'DESC'
    ]
  ];

  public function handleSort($field): void
  {
    if ($field === $this->state['sort_field']) {
      // Change sort direction
      $previousDirection = $this->state['sort_next'][$field];
      $newDirection = $previousDirection === 'ASC' ? 'DESC' : 'ASC';
      $this->state['sort_next'][$field] = $newDirection;
    }
    $this->state['sort_field'] = $field;
    $this->state['sort_direction'] = $this->state['sort_next'][$field];
    $this->getFollowers();
  }

  public function toggleNeverFollow($id): void
  {
    $follower = InstagramUser::whereId($id)->first();
    $follower->never_following = !$follower->never_following;
    if ($follower->never_following) {
    }
    $follower->save();
    $this->getFollowers();
  }

  public function toggleFavorites($id): void
  {
    $follower = InstagramUser::whereId($id)->first();
    $follower->favorite = !$follower->favorite;
    $follower->save();
    $this->getFollowers();
  }

  public function toggleLikes($id): void
  {
    $follower = InstagramUser::whereId($id)->first();
    $follower->active_liker = !$follower->active_liker;
    $follower->save();
    $this->getFollowers();
  }

  public function toggleLeadSource($id): void
  {
    $follower = InstagramUser::whereId($id)->first();
    $follower->lead_source = !$follower->lead_source;
    $follower->save();
    $this->getFollowers();
  }

  public function toggleLeadSourceUsed($id): void
  {
    $follower = InstagramUser::whereId($id)->first();
    $follower->lead_source_used = !$follower->lead_source_used;
    $follower->save();
    $this->getFollowers();
  }

  public function mount(): void
  {
    $this->getFollowers();
  }

  public function render(): View
  {
    return view('livewire.followers');
  }

  private function getFollowers(): void
  {
    $this->state['followers'] = InstagramUser
      ::where('follower', 1)
      ->orderBy($this->state['sort_field'], $this->state['sort_direction'])
      ->get();
    $this->state['followers_count'] = $this->state['followers']->count();
    $this->state['add_following_count'] = InstagramUser
      ::where('add_following', 1)
      ->count();
  }
}
