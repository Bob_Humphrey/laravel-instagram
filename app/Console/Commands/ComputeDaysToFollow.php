<?php

namespace App\Console\Commands;

use App\Actions\ComputeDaysToFollowAction;
use Illuminate\Console\Command;

class ComputeDaysToFollow extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:compute_days';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Compute days to follow';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    ComputeDaysToFollowAction::execute();
  }
}
