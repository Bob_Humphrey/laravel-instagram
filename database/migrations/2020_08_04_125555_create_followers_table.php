<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('followers', function (Blueprint $table) {
      $table->id();
      $table->string('user_name', 100)->index();
      $table->string('name', 100);
      $table->bigInteger('following_id')->index()->default(0);
      $table->boolean('i_follow_them')->index()->default(false);
      $table->boolean('active_liker')->index()->default(false);
      $table->boolean('favorite')->index()->default(false);
      $table->boolean('add_follow_back')->index()->default(false);
      $table->boolean('i_never_follow_them')->index()->default(false);
      $table->boolean('stopped_following_me')->index()->default(false);
      $table->date('started_following_me_date')->index();
      $table->date('confirmed_following_me_date')->nullable($value = true)->default(null)->index();
      $table->date('stopped_following_me_date')->nullable($value = true)->default(null)->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('followers');
  }
}
