<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('likers', function (Blueprint $table) {
      $table->id();
      $table->string('user_name', 100)->index();
      $table->string('name', 100);
      $table->bigInteger('follower_id')->index()->default(0);
      $table->integer('likes');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('likers');
  }
}
