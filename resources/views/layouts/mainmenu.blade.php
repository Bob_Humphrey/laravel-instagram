@php
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
$path = request()->path();
$followers = Str::of($path)->exactly('followers') ? true : false;
$following = Str::of($path)->contains('following') ? true : false;
$results = Str::of($path)->contains('results') ? true : false;
$history = Str::of($path)->contains('history') ? true : false;
$leads = Str::of($path)->contains('lead-list') ? true : false;
$leadSources = Str::of($path)->contains('lead-sources') ? true : false;
$days = Str::of($path)->contains('days') ? true : false;
$instagramUser = Str::of($path)->contains('instagram-user') ? true : false;
$login = Str::of($path)->contains('login') ? true : false;
$home = Str::of($path)->exactly('/') ? true : false;
@endphp

<a href="{{ url('/followers') }}" class="{{ $followers ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Followers
</a>
<a href="{{ url('/following') }}" class="{{ $following ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Following
</a>
<a href="{{ url('/history') }}" class="{{ $history || $home ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  History
</a>
<a href="{{ url('/lead-list') }}" class="{{ $leads ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Leads
</a>
<a href="{{ url('/lead-sources') }}" class="{{ $leadSources ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Lead Sources
</a>
<a href="{{ url('/results') }}" class="{{ $results ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Results
</a>
<a href="{{ url('/days') }}" class="{{ $days ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Days
</a>
<a href="{{ url('/instagram-user') }}"
  class="{{ $instagramUser ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  User
</a>

@guest
  <a href="{{ url('/login') }}" class="{{ $login ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Logout
  </a>
@endauth
