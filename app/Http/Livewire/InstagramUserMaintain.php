<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\InstagramUser;
use Livewire\Component;
use Illuminate\Support\Facades\Log;

class InstagramUserMaintain extends Component
{
  public $instagramUser;
  public $user_name;
  public $follower;
  public $following;
  public $lead_rating;
  public $lead_source;
  public $always_following;
  public $never_following;
  public $favorite;
  public $active_liker;
  public $userNameStatus;
  public $buttonLabel;


  public function saveInstagramUser()
  {

    $instagramUser = InstagramUser::where('user_name', $this->user_name)->first();

    if (empty($instagramUser)) {
      $instagramUser = new InstagramUser();
      $instagramUser->user_name = $this->user_name;
      $instagramUser->name = $this->user_name;
    }

    $instagramUser->follower = $this->follower ? 1 : 0;
    $instagramUser->following = $this->following ? 1 : 0;
    $instagramUser->lead_rating = $this->lead_rating ? $this->lead_rating : 0;
    $instagramUser->lead_source = $this->lead_source ? 1 : 0;
    $instagramUser->always_following = $this->always_following ? 1 : 0;
    $instagramUser->never_following = $this->never_following ? 1 : 0;
    $instagramUser->favorite = $this->favorite ? 1 : 0;
    $instagramUser->active_liker = $this->active_liker ? 1 : 0;

    if ($this->follower) {
      if (is_null($instagramUser->started_follower_date)) {
        $instagramUser->started_follower_date = Carbon::today()->format('Y-m-d');
      }
    }

    if ($this->following) {
      if (is_null($instagramUser->started_following_date)) {
        $instagramUser->started_following_date = Carbon::today()->format('Y-m-d');
      }
    }

    $instagramUser->save();
    $action = $this->buttonLabel === 'Add' ? 'added' : 'updated';
    session()->flash('success', "User $this->user_name has been $action");
    return redirect()->to('/instagram-user');
  }

  public function mount($user_name)
  {
    $this->user_name = $user_name;
    $instagramUser = InstagramUser::where('user_name', $this->user_name)->first();

    if (empty($instagramUser)) {
      $instagramUser = new InstagramUser();
      $this->userNameStatus = 'NEW USER';
      $this->buttonLabel = 'Add';
    } else {
      $this->userNameStatus = 'EXISTING USER';
      $this->buttonLabel = 'Update';
      $this->follower = $instagramUser->follower;
      $this->following = $instagramUser->following;
      $this->lead_rating = $instagramUser->lead_rating;
      $this->lead_source = $instagramUser->lead_source;
      $this->always_following = $instagramUser->always_following;
      $this->never_following = $instagramUser->never_following;
      $this->favorite = $instagramUser->favorite;
      $this->active_liker = $instagramUser->active_liker;
    }
  }

  public function render()
  {
    return view('livewire.instagram-user-maintain');
  }
}
