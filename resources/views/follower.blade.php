@extends('layouts.app')

@section('content')

  <div class=''>

    @if ($errors->any())
      <div class="bg-red-100 rounded-md py-2 px-4 mb-4">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <div class='w-1/2 bg-gray-100 font-nunito_regular text-lg py-10 mt-6 mb-16 mx-auto rounded-md'>

      <h2 class='text-3xl text-center font-nunito_bold text-blue-900 mb-4'>
        Add User
      </h2>

      <form method="POST" action="/followers">

        @csrf

        <div class='grid grid-cols-2 '>
          <label for="username" class='col-span-1 text-right pr-6 py-2'>
            User Name
          </label>
          <div class='col-span-1'>
            <input id="user_name" type="text" class='px-4 py-2' name="user_name" value="{{ old('user_name') }}" required
              autofocus>
          </div>
        </div>

        <div class='grid grid-cols-2 mt-4'>
          <div class='col-start-2 col-span-1'>
            <input type="checkbox" name="properties[]" value="likes"> Likes
          </div>
          <div class='col-start-2 col-span-1'>
            <input type="checkbox" name="properties[]" value="leads"> Lead Source
          </div>
        </div>

        <div class="flex items-center w-full pt-10">
          <button type="submit"
            class='hover:bg-gray-800 hover:text-white text-center font-nunito_bold rounded w-48 py-2 mx-auto border border-gray-700 cursor-pointer'>
            Add
          </button>

        </div>
      </form>

    </div>



  </div>
@endsection
