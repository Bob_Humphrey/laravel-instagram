<?php

namespace App\Actions;

use Carbon\Carbon;
use App\InstagramUser;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class UpdateFollowersAction
{

  public static function execute(): void
  {
    $followerCount = 0;
    $errorCount = 0;
    $readCycleCount = 0;
    $errors = [];
    $instagramUser = self::initializeInstagramUser();

    // Initialize follower flag
    InstagramUser::where('follower', 1)
      ->update(['follower' => 0]);

    $followersFile = Storage::get('data/followers.txt');
    $records = explode("\n", $followersFile);

    foreach ($records as $record) {

      // Profile picture record
      if ($readCycleCount === 0) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $readCycleCount++;
          continue;
        } else {
          $errors[] = $record . ' - PROFILE PICTURE EXPECTED';
          $errorCount++;
          continue;
        }
      }

      // User name record
      if ($readCycleCount === 1) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $errors[] = $record . ' - USER NAME RECORD EXPECTED';
          $errorCount++;
          $readCycleCount = 1;
          $instagramUser = self::initializeInstagramUser();
          continue;
        } else {
          $instagramUser['user_name'] = $record;
          $readCycleCount++;
          continue;
        }
      }

      // Name record
      if ($readCycleCount === 2) {
        $profilePicture = Str::of($record)->contains('profile picture');
        if ($profilePicture) {
          $errors[] = $record . ' - NAME RECORD EXPECTED';
          $errorCount++;
          $instagramUser['name'] = $instagramUser['user_name'];
          self::save($instagramUser);
          $followerCount++;
          $instagramUser = self::initializeInstagramUser();
          $readCycleCount = 1;
          continue;
        } else {
          $instagramUser['name'] = $record;
          self::save($instagramUser);
          $followerCount++;
          $readCycleCount = 0;
          continue;
        }
      }
    }
  }

  private static function initializeInstagramUser(): array
  {
    $instagramUser = ['name' => '', 'user_name' => ''];
    return $instagramUser;
  }

  private static function save($textFileInstagramUser): void
  {

    $instagramUser = InstagramUser::where('user_name', $textFileInstagramUser['user_name'])->first();

    if (empty($instagramUser)) {
      $instagramUser = new InstagramUser();
    }

    if (is_null($instagramUser->started_follower_date)) {
      $instagramUser->started_follower_date = Carbon::today()->format('Y-m-d');
    }

    $instagramUser->follower = true;
    $instagramUser->ever_follower = true;
    $instagramUser->name = $textFileInstagramUser['name'];
    $instagramUser->user_name = $textFileInstagramUser['user_name'];
    $instagramUser->confirmed_follower_date =
      Carbon::today()->format('Y-m-d');
    $instagramUser->save();
  }
}
