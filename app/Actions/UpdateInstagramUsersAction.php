<?php

namespace App\Actions;

use App\InstagramUser;
use Illuminate\Support\Carbon;

class UpdateInstagramUsersAction
{
  // Who followed first?
  public const ME = 0;
  public const THEM = 1;
  public const SAME = 2;
  public const NEITHER = 3;
  public const UNKNOWN = 4;

  // Days before I stop following
  public const STOP_FOLLOWING_DAYS = 11;

  public static function execute()
  {
    InstagramUser::chunk(200, function ($instagramUsers) {
      foreach ($instagramUsers as $instagramUser) {

        // lost_follower

        if (
          $instagramUser->following &&
          $instagramUser->ever_follower &&
          !$instagramUser->follower
        ) {
          $instagramUser->lost_follower = true;
        } else {
          $instagramUser->lost_follower = false;
        }

        // add_following

        if (
          $instagramUser->follower &&
          !$instagramUser->following &&
          !$instagramUser->never_following &&
          !$instagramUser->lost_follower
        ) {
          $instagramUser->add_following = true;
        } else {
          $instagramUser->add_following = false;
        }

        // stop_following - never became follower

        if (
          $instagramUser->following &&
          !$instagramUser->follower &&
          !$instagramUser->always_following &&
          ((Carbon::createFromDate($instagramUser->started_following_date)->addDays(self::STOP_FOLLOWING_DAYS))->lessThan(Carbon::today()))
        ) {
          $instagramUser->stop_following = true;
        } else {
          $instagramUser->stop_following = false;
        }

        $instagramUser->followed_first = self::determineWhoFollowedFirst($instagramUser);

        // stopped_follower_date

        if (
          $instagramUser->ever_follower &&
          !$instagramUser->follower &&
          is_null($instagramUser->stopped_follower_date)
        ) {
          $instagramUser->stopped_follower_date = Carbon::today()->format('Y-m-d');
        }

        // stopped_following_date

        if (
          $instagramUser->ever_following &&
          !$instagramUser->following &&
          is_null($instagramUser->stopped_following_date)
        ) {
          $instagramUser->stopped_following_date = Carbon::today()->format('Y-m-d');
        }

        $instagramUser->save();
      }
    });
  }

  protected static function determineWhoFollowedFirst($instagramUser)
  {
    if (!$instagramUser->ever_follower && !$instagramUser->ever_following) {
      return self::NEITHER;
    }

    if (is_null($instagramUser->started_follower_date)) {
      return self::UNKNOWN;
    }

    if (is_null($instagramUser->started_following_date)) {
      return self::UNKNOWN;
    }

    if ($instagramUser->started_follower_date == '2020-01-01') {
      return self::UNKNOWN;
    }

    if ($instagramUser->started_following_date == '2020-01-01') {
      return self::UNKNOWN;
    }

    if ($instagramUser->started_follower_date < $instagramUser->started_following_date) {
      return self::THEM;
    }

    if ($instagramUser->started_follower_date > $instagramUser->started_following_date) {
      return self::ME;
    }

    if ($instagramUser->started_follower_date === $instagramUser->started_following_date) {
      return self::SAME;
    }
  }
}
