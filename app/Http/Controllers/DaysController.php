<?php

namespace App\Http\Controllers;

use App\Day;
use Illuminate\View\View;
use Illuminate\Http\Request;

class DaysController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request): View
  {
    $days = Day::get();
    return view('days', ['days' => $days]);
  }
}
