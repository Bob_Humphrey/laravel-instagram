<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('instagram_users', function (Blueprint $table) {
      $table->id();
      $table->string('user_name', 100)->index();
      $table->string('name', 100);
      $table->boolean('follower')->index()->default(false);
      $table->boolean('following')->index()->default(false);
      $table->boolean('ever_follower')->index()->default(false);
      $table->boolean('ever_following')->index()->default(false);
      $table->boolean('always_following')->index()->default(false);
      $table->boolean('never_following')->index()->default(false);
      $table->boolean('favorite')->index()->default(false);
      $table->boolean('active_liker')->index()->default(false);
      $table->boolean('lead_source')->index()->default(false);
      $table->boolean('lead_source_used')->index()->default(false);
      $table->boolean('add_following')->index()->default(false);
      $table->boolean('stop_following')->index()->default(false);
      $table->boolean('lost_follower')->index()->default(false);
      $table->tinyInteger('followed_first')->index()->default(0);
      $table->boolean('current_lead')->index()->default(false);
      $table->boolean('lead_rating')->index()->default(false);
      $table->boolean('lead_reject')->index()->default(false);
      $table->boolean('current_lead_stopping_point')->index()->default(false);
      $table->date('started_follower_date')->nullable($value = true)->default(null)->index();
      $table->date('confirmed_follower_date')->nullable($value = true)->default(null)->index();
      $table->date('stopped_follower_date')->nullable($value = true)->default(null)->index();
      $table->date('started_following_date')->nullable($value = true)->default(null)->index();
      $table->date('confirmed_following_date')->nullable($value = true)->default(null)->index();
      $table->date('stopped_following_date')->nullable($value = true)->default(null)->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('instagram_users');
  }
}
