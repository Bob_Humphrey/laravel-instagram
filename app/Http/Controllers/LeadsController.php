<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LeadsController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request): View
  {
    $records = Lead::orderBy('id')->get();
    return view('leads', ['records' => $records]);
  }
}
