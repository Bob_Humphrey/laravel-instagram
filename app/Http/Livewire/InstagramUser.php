<?php

namespace App\Http\Livewire;

use Livewire\Component;


class InstagramUser extends Component
{
  public $user_name;

  public function getInstagramUser()
  {
    $user_name = trim($this->user_name);

    return redirect()->to('/instagram-user/' . $user_name);
  }

  public function render()
  {
    return view('livewire.instagram-user');
  }
}
