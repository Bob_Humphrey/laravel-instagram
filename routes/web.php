<?php

use App\Actions\GetLeadsAction;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HistoryController::class)->name('home');

Route::get('/login', \App\Http\Livewire\Login::class)->name('login');

Route::get('/logout', \App\Http\Controllers\LogoutController::class)->name('logout');

Route::get('/following', \App\Http\Livewire\Following::class)->name('following')->middleware('auth');

Route::get('/followers', \App\Http\Livewire\Followers::class)->name('followers')->middleware('auth');

Route::get('/results', \App\Http\Livewire\Results::class)->name('results')->middleware('auth');

Route::get('/history', \App\Http\Controllers\HistoryController::class)->name('history');

Route::get('/lead-list', \App\Http\Livewire\LeadList::class)->name('lead-list')->middleware('auth');

Route::get('/lead-sources', \App\Http\Livewire\LeadSources::class)->name('lead-sources')->middleware('auth');

Route::get('/days', \App\Http\Controllers\DaysController::class)->name('days')->middleware('auth');

Route::get('/instagram-user', \App\Http\Livewire\InstagramUser::class)->name('instagram-user')->middleware('auth');

Route::get('/instagram-user/{user_name}', \App\Http\Livewire\InstagramUserMaintain::class)->name('instagram-user-maintain')->middleware('auth');

Route::get('/refresh-leads', function () {
  GetLeadsAction::execute();
  return redirect()->route('lead-list');
})->middleware('auth');
