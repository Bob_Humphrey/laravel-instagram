<div class="my-10 flex justify-center w-full">
  <section class="rounded p-4 w-6/12 bg-gray-200">
    <h2 class="text-3xl text-center text-blue-800 font-nunito_bold mb-4">
      Login
    </h2>
    <div class="w-full text-red-600 text-center mt-4">{{ $error }}</div>
    <form class="my-4" wire:submit.prevent="submit">
      @csrf
      <div class="flex justify-around my-8">
        <div class="flex flex-wrap w-10/12">
          <input type="email" class="p-2 rounded w-full" placeholder="Email" wire:model="email" />
          @error('email')
          <span class="text-red-500 text-xs mt-1">{{ $message }}</span> @enderror
        </div>
      </div>
      <div class="flex justify-around my-8">
        <div class="flex flex-wrap w-10/12">
          <input type="password" class="p-2 w-full" placeholder="Password" wire:model="password" />
          @error('password')
          <span class="text-red-500 text-xs mt-1">{{ $message }}</span> @enderror
        </div>
      </div>
      <div class="flex justify-around my-8">
        <div class="flex flex-wrap w-10/12">
          <input type="submit" value="Login"
            class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer" />
        </div>
      </div>
    </form>
  </section>
</div>
