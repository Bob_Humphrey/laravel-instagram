<?php

namespace App\Actions;

use App\Follower;
use Illuminate\Support\Carbon;

class AddFollowerAction
{
  public static function execute($request): void
  {
    $existingFollower = Follower::where('user_name', $request->user_name)->first();
    // New follower
    if (empty($existingFollower)) {
      $newFollower = new Follower;
      $newFollower->name = '';
      $newFollower->user_name = $request->user_name;
      if (in_array('likes', $request->get('properties'))) {
        $newFollower->active_liker = true;
      }
      if (in_array('leads', $request->get('properties'))) {
        $newFollower->lead_source = true;
      }
      $newFollower->save();
    }
    // Existing follower 
    else {
      if (in_array('likes', $request->get('properties'))) {
        $existingFollower->active_liker = true;
      }
      if (in_array('leads', $request->get('properties'))) {
        $existingFollower->lead_source = true;
      }
      $existingFollower->confirmed_following_me_date = Carbon::today()->format('Y-m-d');
      $existingFollower->save();
    }
  }
}
