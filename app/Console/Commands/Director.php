<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Director extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:director';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Calls all of the other commands.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $this->call('app:update_followers');
    $this->call('app:update_following');
    $this->call('app:update_instagram_users');
    $this->call('app:update_history');
    $this->call('app:compute_days');
  }
}
