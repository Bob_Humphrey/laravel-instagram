<div class="my-10">
  <x-alert type="success" class="w-3/5 bg-green-700 text-white p-4 mb-4 mx-auto" />

  <section class="rounded py-8 px-16 w-3/5 bg-gray-200 mx-auto">

    <h2 class="text-3xl text-center text-blue-800 font-nunito_bold mb-8">
      Instagram User
    </h2>

    {{-- USER NAME --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="user_name" />
      </div>
      <div class="col-span-7">
        <x-input name="user_name" wire:model="user_name"
          class="p-2 rounded border border-gray-200 w-full appearance-none" />
      </div>
    </div>

    <div class="grid grid-cols-12 mb-8">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="user_name" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center">
      <button type="submit" wire:click="getInstagramUser"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Find
      </button>
    </div>

  </section>

</div>
