<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowingsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('followings', function (Blueprint $table) {
      $table->id();
      $table->string('user_name', 100)->index();
      $table->string('name', 100);
      $table->bigInteger('follower_id')->index()->default(0);
      $table->boolean('they_follow_me')->index()->default(false);
      $table->boolean('add_stop_following')->index()->default(false);
      $table->boolean('never_stop_following')->index()->default(false);
      $table->boolean('stopped_following_them')->index()->default(false);
      $table->date('started_following_them_date')->index();
      $table->date('confirmed_following_them_date')->nullable($value = true)->default(null)->index();
      $table->date('stopped_following_them_date')->nullable($value = true)->default(null)->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('followings');
  }
}
