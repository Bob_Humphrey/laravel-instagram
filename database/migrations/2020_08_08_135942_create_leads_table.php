<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('leads', function (Blueprint $table) {
      $table->id();
      $table->string('user_name', 100)->index();
      $table->string('name', 100);
      $table->bigInteger('following_id')->index()->default(0);
      $table->boolean('finished')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('leads');
  }
}
