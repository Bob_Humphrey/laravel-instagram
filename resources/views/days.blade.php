@extends('layouts.app')

@section('content')

  @php
  use Carbon\Carbon;
  @endphp

  <div class="w-1/2 justify-center py-6 mx-auto">
    <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
      Followback Days
    </h2>

    <table class="w-full">
      @foreach ($days as $day)

        @if ($loop->index % 14 === 0)
          <tr class="grid grid-cols-2 bg-blue-50 text-sm font-nunito_bold border-b border-gray-300">
            <th class="col-span-1 text-left py-3 pl-3">Days</th>
            <th class="col-span-1 text-right py-3 pr-3">Followers</th>
          </tr>
        @endif

        <tr
          class="grid grid-cols-2 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? '' : 'bg-gray-50' }}">
          <td class="col-span-1 text-left py-3 pl-3">
            {{ $day->key }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $day->value }}
          </td>
        </tr>
      @endforeach
    </table>
  </div>

@endsection
